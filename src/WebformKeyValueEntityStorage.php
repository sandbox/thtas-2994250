<?php

namespace Drupal\webform_config_key_value;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\webform\WebformEntityStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebformKeyValueEntityStorage extends WebformEntityStorage {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * WebformKeyValueEntityStorage constructor.
   *
   * @param EntityTypeInterface $entity_type
   * @param ConfigFactoryInterface $config_factory
   * @param UuidInterface $uuid_service
   * @param LanguageManagerInterface $language_manager
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $key_value_store
   */
  public function __construct(EntityTypeInterface $entity_type, ConfigFactoryInterface $config_factory, UuidInterface $uuid_service, LanguageManagerInterface $language_manager, Connection $database, KeyValueStoreInterface $key_value_store) {
    parent::__construct($entity_type, $config_factory, $uuid_service, $language_manager, $database);
    $this->keyValueStore = $key_value_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('config.factory'),
      $container->get('uuid'),
      $container->get('language_manager'),
      $container->get('database'),
      $container->get('keyvalue')->get('entity_storage__' . $entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, EntityInterface $entity) {
    $is_new = $entity->isNew();

    // Save the entity data in the key value store.
    $this->keyValueStore->set($entity->id(), $entity->toArray());

    // If this is a rename, delete the original entity.
    if ($this->has($id, $entity) && $id !== $entity->id()) {
      $this->keyValueStore->delete($id);
    }

    return $is_new ? SAVED_NEW : SAVED_UPDATED;
  }

  /**
   * {@inheritdoc}
   */
  public function doDelete($entities) {
    $entity_ids = array_keys($entities);
    $this->keyValueStore->deleteMultiple($entity_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function doLoadMultiple(array $ids = NULL) {
    if (empty($ids)) {
      $entities = $this->keyValueStore->getAll();
    }
    else {
      $entities = $this->keyValueStore->getMultiple($ids);
    }
    return $this->mapFromStorageRecords($entities);
  }

  /**
   * {@inheritdoc}
   */
  protected function has($id, EntityInterface $entity) {
    return $this->keyValueStore->has($id);
  }

  /**
   * {@inheritdoc}
   */
  public function hasData() {
    return (bool) $this->keyValueStore->getAll();
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'entity.query.keyvalue';
  }

}